const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const port = process.argv[2] || 9000;

http.createServer((req, res) => {

    const parsedUrl = url.parse(req.url);
    let pathname = `.${parsedUrl.pathname}`;
    const ext = path.parse(pathname).ext;
    const map = {
        '.ico': 'image/x-icon',
        '.html': 'text/html',
        '.js': 'text/javascript',
        '.json': 'application/json',
        '.css': 'text/css',
        '.png': 'image/png',
        '.jpg': 'image/jpeg',
        '.wav': 'audio/wav',
        '.mp3': 'audio/mpeg',
        '.svg': 'image/svg+xml',
        '.pdf': 'application/pdf',
        '.doc': 'application/msword'
    };

    fs.exists(pathname, (exist) => {
        if (!exist) {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify({
                status: 'error',
                message: 'File '+ pathname +' not found!'
            }));
            return;
        }

        if (fs.statSync(pathname).isDirectory()) {

            fs.readdir(pathname, (err, files) => {
                if (err) {
                    res.setHeader('Content-Type', 'application/json');
                    res.end(JSON.stringify({
                        status: 'error',
                        message: 'Error loading the directory: ' + err.message
                    }));
                } else {
                    res.setHeader('Content-Type', 'application/json');
                    let output = {
                        status: 'success',
                        files: [],
                        folders: []
                    };
                    files.forEach(function (file) {
                        let fullname = path.join(pathname, file);
                        if (fs.statSync(fullname).isDirectory()) {
                            output.folders.push(fullname);
                        } else {
                            output.files.push(fullname);
                        }
                        console.log(file);
                    });
                    res.end(JSON.stringify(output));
                }
            });

        } else {
            fs.readFile(pathname, (err, data) => {
                if (err) {
                    res.statusCode = 500;
                    res.end(`Error getting the file: ${err}.`);
                } else {
                    res.setHeader('Content-type', map[ext] || 'text/plain');
                    res.end(data);
                }
            });
        }
    });

}).listen(parseInt(port));

console.log('Server listening on port ' + port);